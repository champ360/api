-- match
CREATE TABLE `match` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `id_championship` INT(11) NULL,
    `id_user_home` INT(11) NOT NULL,
    `id_user_away` INT(11) NOT NULL,
    `id_team_home` INT(11) NOT NULL,
    `id_team_away` INT(11) NOT NULL,
    `creation_date` DATE NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_championship`) REFERENCES championship(`id`),
    FOREIGN KEY (`id_user_home`) REFERENCES user(`id`),
    FOREIGN KEY (`id_user_away`) REFERENCES user(`id`),
    FOREIGN KEY (`id_team_home`) REFERENCES team(`id`),
    FOREIGN KEY (`id_team_away`) REFERENCES team(`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

