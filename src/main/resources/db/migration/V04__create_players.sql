-- players
CREATE TABLE `player` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
	`nationality` VARCHAR(20) NOT NULL,
	`image` VARCHAR(200) NULL,
    `id_team` INT(11) NULL,
    `creation_date` DATE NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_team`) REFERENCES team(`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;
