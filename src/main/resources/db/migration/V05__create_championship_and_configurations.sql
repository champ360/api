-- championship_platform
CREATE TABLE `championship_platform` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

-- championship_videogame
CREATE TABLE `championship_videogame` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

-- championship_system
CREATE TABLE `championship_system` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

-- championship_format
CREATE TABLE `championship_format` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

-- championship
CREATE TABLE `championship` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    `id_championship_platform` INT(11) NOT NULL,
    `id_championship_videogame` INT(11) NOT NULL,
    `id_championship_system` INT(11) NOT NULL,
    `id_championship_format` INT(11) NOT NULL,
    `number_participants` INT(5) NOT NULL,
    `image` VARCHAR(200) NULL,
    `creation_date` DATE NOT NULL,
    PRIMARY KEY (`id`),
	FOREIGN KEY (`id_championship_platform`) REFERENCES championship_platform(`id`),
    FOREIGN KEY (`id_championship_videogame`) REFERENCES championship_videogame(`id`),
    FOREIGN KEY (`id_championship_system`) REFERENCES championship_system(`id`),
    FOREIGN KEY (`id_championship_format`) REFERENCES championship_format(`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

-- insert platform
INSERT INTO `championship_platform` (`id`, `name`) VALUES (1, 'PS4');
INSERT INTO `championship_platform` (`id`, `name`) VALUES (2, 'XBOX ONE');

-- insert videogame
INSERT INTO `championship_videogame` (`id`, `name`) VALUES (1, 'FIFA 19');
INSERT INTO `championship_videogame` (`id`, `name`) VALUES (2, 'PES 19');

-- insert system
INSERT INTO `championship_system` (`id`, `name`) VALUES (1, 'Pontos corridos');
INSERT INTO `championship_system` (`id`, `name`) VALUES (2, 'Mata-mata');

-- insert format
INSERT INTO `champ360`.`championship_format` (`id`, `name`) VALUES (1, 'Jogo único');
INSERT INTO `champ360`.`championship_format` (`id`, `name`) VALUES (2, 'Ida e volta');
