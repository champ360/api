-- match goals
CREATE TABLE `match_goals` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `id_match` INT(11) NOT NULL,
    `id_user` INT(11) NOT NULL,
    `id_player` INT(11) NOT NULL,
    `number_goals` INT(11) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_match`) REFERENCES `match`(`id`),
    FOREIGN KEY (`id_user`) REFERENCES user(`id`),
    FOREIGN KEY (`id_player`) REFERENCES player(`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

