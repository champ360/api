-- user and role
CREATE TABLE `user` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `password` VARCHAR(150) NOT NULL,
    `status` BOOLEAN NOT NULL,
    `creation_date` DATE NOT NULL,
    PRIMARY KEY (`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

CREATE TABLE role (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
    `description` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE user_role (
	`id_user` INT(11) NOT NULL,
    `id_role` INT(11) NOT NULL,
    PRIMARY KEY (`id_user`, `id_role`),
    FOREIGN KEY (`id_user`) REFERENCES user(`id`),
    FOREIGN KEY (`id_role`) REFERENCES role(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `user` (`id`, `name`, `email`, `password`, `status`, `creation_date`)
VALUES (1, 'Ewerton Dias', 'es-dias@hotmail.com', '$2a$10$BELY1V7nX8d3.Jt4nTIaKuPfB5QnpLvgj6U4HqFsHMNYBFfW/CpD2', 1, '2019-01-22');

INSERT INTO `role` (`id`, `description`) VALUES (1, 'ROLE_ADMIN');
INSERT INTO `role` (`id`, `description`) VALUES (2, 'ROLE_USER');

INSERT INTO `user_role` (`id_user`, `id_role`) VALUES (1, 1);
INSERT INTO `user_role` (`id_user`, `id_role`) VALUES (1, 2);
