-- match stats
CREATE TABLE `match_stats` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `id_match` INT(11) NOT NULL,
    `id_user` INT(11) NOT NULL,
    `match_result` TINYINT(1) NOT NULL COMMENT '0 = Derrota, 1 = Vitoria, 2 = Empate',
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_match`) REFERENCES `match`(`id`),
    FOREIGN KEY (`id_user`) REFERENCES user(`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

