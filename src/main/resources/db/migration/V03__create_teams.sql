-- team
CREATE TABLE `team` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    `acronym` CHAR(3) NOT NULL,
    `formation` VARCHAR(10) NOT NULL,
    `image` VARCHAR(200) NULL,
    `id_official_league` INT(11) NOT NULL,
    `creation_date` DATE NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_official_league`) REFERENCES official_league(`id`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;
