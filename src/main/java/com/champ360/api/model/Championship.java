package com.champ360.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Championship implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	private String name;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_championship_format")
	private ChampionshipFormat format;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_championship_platform")
	private ChampionshipPlatform platform;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_championship_system")
	private ChampionshipSystem system;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_championship_videogame")
	private ChampionshipVideogame videogame;

	@JsonIgnoreProperties("championship")
	@OneToMany(mappedBy = "championship")
	private List<Match> matches;
	
	@NotNull
	private Integer numberParticipants;
	
	private LocalDate creationDate;
	
	@PrePersist
	public void onPrePersist() {
		this.setCreationDate(LocalDate.now());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ChampionshipFormat getFormat() {
		return format;
	}

	public void setFormat(ChampionshipFormat format) {
		this.format = format;
	}

	public ChampionshipPlatform getPlatform() {
		return platform;
	}

	public void setPlatform(ChampionshipPlatform platform) {
		this.platform = platform;
	}

	public ChampionshipSystem getSystem() {
		return system;
	}

	public void setSystem(ChampionshipSystem system) {
		this.system = system;
	}

	public ChampionshipVideogame getVideogame() {
		return videogame;
	}

	public void setVideogame(ChampionshipVideogame videogame) {
		this.videogame = videogame;
	}

	public List<Match> getMatches() {
		return matches;
	}

	public void setMatches(List<Match> matches) {
		this.matches = matches;
	}

	public Integer getNumberParticipants() {
		return numberParticipants;
	}

	public void setNumberParticipants(Integer numberParticipants) {
		this.numberParticipants = numberParticipants;
	}
	
	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "Championship [id=" + id + ", format=" + format + ", platform=" + platform + ", system=" + system
				+ ", videogame=" + videogame + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Championship other = (Championship) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
