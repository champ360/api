package com.champ360.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "match", catalog = "champ360")
@NamedEntityGraph(name = "match-load-championship",
	attributeNodes = @NamedAttributeNode("championship"))
public class Match implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_championship")
	private Championship championship;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_user_home")
	private UserModel userHome;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_user_away")
	private UserModel userAway;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_team_home")
	private Team teamHome;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_team_away")
	private Team teamAway;
	
	@JsonIgnoreProperties("match")
	@OneToMany(mappedBy = "match", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<MatchGoals> goals;
	
	@JsonIgnoreProperties("match")
	@OneToMany(mappedBy = "match", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<MatchStats> stats;
	
	@Transient
	private Integer totalGoalsHome;

	@Transient
	private Integer totalGoalsAway;	
	
	private LocalDate creationDate;

	@PrePersist
	public void onPrePersist() {
		this.setCreationDate(LocalDate.now());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Championship getChampionship() {
		return championship;
	}

	public void setChampionship(Championship championship) {
		this.championship = championship;
	}

	public UserModel getUserHome() {
		return userHome;
	}

	public void setUserHome(UserModel userHome) {
		this.userHome = userHome;
	}

	public UserModel getUserAway() {
		return userAway;
	}

	public void setUserAway(UserModel userAway) {
		this.userAway = userAway;
	}
	
	public Team getTeamHome() {
		return teamHome;
	}

	public void setTeamHome(Team teamHome) {
		this.teamHome = teamHome;
	}

	public Team getTeamAway() {
		return teamAway;
	}

	public void setTeamAway(Team teamAway) {
		this.teamAway = teamAway;
	}

	public List<MatchGoals> getGoals() {
		return goals;
	}

	public void setGoals(List<MatchGoals> goals) {
		this.goals = goals;
	}

	public List<MatchStats> getStats() {
		return stats;
	}

	public void setStats(List<MatchStats> stats) {
		this.stats = stats;
	}
	
	public Integer getTotalGoalsHome() {
		return totalGoalsHome;
	}

	public void setTotalGoalsHome(Integer totalGoalsHome) {
		this.totalGoalsHome = totalGoalsHome;
	}

	public Integer getTotalGoalsAway() {
		return totalGoalsAway;
	}

	public void setTotalGoalsAway(Integer totalGoalsAway) {
		this.totalGoalsAway = totalGoalsAway;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "Match [id=" + id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Match other = (Match) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
