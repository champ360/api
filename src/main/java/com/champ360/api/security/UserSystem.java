package com.champ360.api.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.champ360.api.model.UserModel;

public class UserSystem extends User {

	private static final long serialVersionUID = 1L;

	private UserModel user;

	public UserSystem(UserModel user, Collection<? extends GrantedAuthority> authorities) {
		super(user.getEmail(), user.getPassword(), authorities);
		this.user = user;
	}

	public UserModel getUser() {
		return user;
	}

}
