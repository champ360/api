package com.champ360.api.exception;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ResourceExceptionHandler {

	private final Logger logger = LoggerFactory.getLogger(ResourceExceptionHandler.class);
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<StandardErrorException> userNotFoundException(ResourceNotFoundException e, HttpServletRequest request) {
		logger.error("Erro na rota: {}, {}", request.getRequestURI(), e.getMessage());
		
		StandardErrorException error = new StandardErrorException(e.getHttpStatus().value(), e.getMessage(), System.currentTimeMillis());
				
		return ResponseEntity.status(e.getHttpStatus().value()).body(error);
	}
	
}
