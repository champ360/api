package com.champ360.api.exception;

public class StandardErrorException {

	private Integer httpStatus;
	private String message;
	private Long timestamp;

	public StandardErrorException(Integer status, String message, Long timestamp) {
		this.httpStatus = status;
		this.message = message;
		this.timestamp = timestamp;
	}

	public Integer getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(Integer httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

}
