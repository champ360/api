package com.champ360.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.champ360.api.exception.ResourceNotFoundException;
import com.champ360.api.model.ChampionshipSystem;
import com.champ360.api.repository.ChampionshipSystemRepository;

@Service
public class ChampionshipSystemService {

	@Autowired
	private ChampionshipSystemRepository championshipSystemRepository;
	
	public ChampionshipSystem findById(Long id) {
		Optional<ChampionshipSystem> championshipSystem = championshipSystemRepository.findById(id);
		
		if (!championshipSystem.isPresent()) {
			// TODO: Adicionar a mensagem no arquivo de mensagens
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "Sistema do campeonato não encontrado");
		}
		
		return championshipSystem.get();
	}
	
}
