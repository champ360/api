package com.champ360.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.champ360.api.exception.ResourceNotFoundException;
import com.champ360.api.model.Player;
import com.champ360.api.model.Team;
import com.champ360.api.repository.PlayerRepository;

@Service
public class PlayerService {

	@Autowired
	private PlayerRepository playerRepository;
	
	@Autowired
	private TeamService teamService;
	
	public List<Player> list() {
		return playerRepository.findAll();
	}
	
	public Player findById(Long id) {
		Optional<Player> player = playerRepository.findById(id);
		
		if (!player.isPresent()) {
			// TODO: Adicionar a mensagem no arquivo de mensagens
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "Jogador não encontrado");
		}
		
		return player.get();
	}
	
	public Player save(Player player) {
		if (player.getTeam() != null) {
			Team team = teamService.findById(player.getTeam().getId());
			player.setTeam(team);
		}
		
		return playerRepository.save(player);
	}
	
	public Player update(Long id, Player newPlayer) {
		Player player = findById(id);
		
		Team team = teamService.findById(newPlayer.getTeam().getId());
		newPlayer.setTeam(team);
		
		BeanUtils.copyProperties(newPlayer, player, "id");
		
		return playerRepository.save(player);
	}

}
