package com.champ360.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.champ360.api.exception.ResourceNotFoundException;
import com.champ360.api.model.ChampionshipPlatform;
import com.champ360.api.repository.ChampionshipPlatformRepository;

@Service
public class ChampionshipPlatformService {

	@Autowired
	private ChampionshipPlatformRepository championshipPlatformRepository;
	
	public ChampionshipPlatform findById(Long id) {
		Optional<ChampionshipPlatform> championshipPlatform = championshipPlatformRepository.findById(id);
		
		if (!championshipPlatform.isPresent()) {
			// TODO: Adicionar a mensagem no arquivo de mensagens
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "Plataforma do campeonato não encontrada");
		}
		
		return championshipPlatform.get();
	}
	
}
