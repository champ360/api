package com.champ360.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.champ360.api.exception.ResourceNotFoundException;
import com.champ360.api.model.OfficialLeague;
import com.champ360.api.model.Team;
import com.champ360.api.repository.TeamRepository;

@Service
public class TeamService {

	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private OfficialLeagueService officialLeagueService;
	
	public List<Team> list() {
		return teamRepository.findAll();
	}
	
	public Team findById(Long id) {
		Optional<Team> team = teamRepository.findById(id);
		
		if (!team.isPresent()) {
			// TODO: Adicionar a mensagem no arquivo de mensagens
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "Equipe não encontrada");
		}
		
		return team.get();
	}
	
	public Team save(Team team) {
		OfficialLeague officialLeague = officialLeagueService.findById(team.getOfficialLeague().getId());
		team.setOfficialLeague(officialLeague);

		return teamRepository.save(team);
	}
	
	public Team update(Long id, Team newTeam) {
		Team team = findById(id);
		
		OfficialLeague officialLeague = officialLeagueService.findById(newTeam.getOfficialLeague().getId());
		newTeam.setOfficialLeague(officialLeague);
		
		BeanUtils.copyProperties(newTeam, team, "id");

		return teamRepository.save(team);
	}
	
}
