package com.champ360.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.champ360.api.exception.ResourceNotFoundException;
import com.champ360.api.model.ChampionshipVideogame;
import com.champ360.api.repository.ChampionshipVideogameRepository;

@Service
public class ChampionshipVideogameService {

	@Autowired
	private ChampionshipVideogameRepository championshipVideogameRepository;
	
	public ChampionshipVideogame findById(Long id) {
		Optional<ChampionshipVideogame> championshipVideogame = championshipVideogameRepository.findById(id);
		
		if (!championshipVideogame.isPresent()) {
			// TODO: Adicionar a mensagem no arquivo de mensagens
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "Videogame do campeonato não encontrado");
		}
		
		return championshipVideogame.get();
	}
	
}
