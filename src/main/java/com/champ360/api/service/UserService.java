package com.champ360.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.champ360.api.exception.ResourceNotFoundException;
import com.champ360.api.model.UserModel;
import com.champ360.api.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public List<UserModel> list() {
		return userRepository.findAll();
	}
	
	public UserModel findById(Long id) {
		Optional<UserModel> user = userRepository.findById(id);
		
		if (!user.isPresent()) {
			// TODO: Adicionar a mensagem no arquivo de mensagens
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "Usuário " + id + " não encontrado") ;
		}
		
		return user.get();
	}
	
	public UserModel save(UserModel user) {
		user.setPassword(encodePassword(user.getPassword()));
		
		return userRepository.save(user);
	}
	
	public UserModel update(Long id, UserModel newUser) {
		UserModel user = findById(id);
		BeanUtils.copyProperties(newUser, user, "id");

		user.setPassword(encodePassword(user.getPassword()));
		
		return userRepository.save(user);
	}
	
	public void delete(Long id) {
		userRepository.deleteById(id);
	}
	
	private String encodePassword(String password) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(password);
	}
	
}
