package com.champ360.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.champ360.api.exception.ResourceNotFoundException;
import com.champ360.api.model.ChampionshipFormat;
import com.champ360.api.repository.ChampionshipFormatRepository;

@Service
public class ChampionshipFormatService {

	@Autowired
	private ChampionshipFormatRepository championshipFormatRepository;
	
	public ChampionshipFormat findById(Long id) {
		Optional<ChampionshipFormat> championshipFormat = championshipFormatRepository.findById(id);
		
		if (!championshipFormat.isPresent()) {
			// TODO: Adicionar a mensagem no arquivo de mensagens
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "Formato do campeonato não encontrado");
		}
		
		return championshipFormat.get();
	}
	
}
