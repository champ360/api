package com.champ360.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.champ360.api.exception.ResourceNotFoundException;
import com.champ360.api.model.OfficialLeague;
import com.champ360.api.repository.OfficialLeagueRepository;

@Service
public class OfficialLeagueService {

	@Autowired
	private OfficialLeagueRepository officialLeagueRepository;
	
	public OfficialLeague findById(Long id) {
		Optional<OfficialLeague> officialLeague = officialLeagueRepository.findById(id);
		
		if (!officialLeague.isPresent()) {
			// TODO: Adicionar a mensagem no arquivo de mensagens
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "Liga oficial não encontrada");
		}
		
		return officialLeague.get();
	}
	
}
