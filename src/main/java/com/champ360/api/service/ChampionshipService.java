package com.champ360.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.champ360.api.model.Championship;
import com.champ360.api.model.ChampionshipFormat;
import com.champ360.api.model.ChampionshipPlatform;
import com.champ360.api.model.ChampionshipSystem;
import com.champ360.api.model.ChampionshipVideogame;
import com.champ360.api.repository.ChampionshipRepository;

@Service
public class ChampionshipService {

	@Autowired
	private ChampionshipRepository championshipRepository;
	
	@Autowired
	private ChampionshipFormatService championshipFormatService;

	@Autowired
	private ChampionshipPlatformService championshipPlatformService;

	@Autowired
	private ChampionshipSystemService championshipSystemService;

	@Autowired
	private ChampionshipVideogameService championshipVideogameService;
	
	public List<Championship> list() {
		return championshipRepository.findAll();
	}
	
	public Championship save(Championship championship) {
		ChampionshipFormat championshipFormat = championshipFormatService.findById(championship.getFormat().getId());
		championship.setFormat(championshipFormat);
		
		ChampionshipPlatform championshipPlatform = championshipPlatformService.findById(championship.getPlatform().getId());
		championship.setPlatform(championshipPlatform);
		
		ChampionshipSystem championshipSystem = championshipSystemService.findById(championship.getSystem().getId());
		championship.setSystem(championshipSystem);
		
		ChampionshipVideogame championshipVideogame = championshipVideogameService.findById(championship.getVideogame().getId());
		championship.setVideogame(championshipVideogame);
		
		return championshipRepository.save(championship);
	}
	
}
