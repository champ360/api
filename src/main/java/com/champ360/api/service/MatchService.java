package com.champ360.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.champ360.api.dto.RoundDTO;
import com.champ360.api.model.Match;
import com.champ360.api.model.MatchGoals;
import com.champ360.api.model.MatchStats;
import com.champ360.api.model.UserModel;
import com.champ360.api.repository.MatchRepository;

@Service
public class MatchService {

	@Autowired
	private MatchRepository matchRepository;
	
	@Autowired
	private UserService userService;
	
	public List<Match> list() {
		List<Match> matches = matchRepository.findAll();
		
		// Include total goals in matches
		this.totalGoals(matches);
		
		return matches;
	}
	
	public Match save(Match match) {
		UserModel userHome = userService.findById(match.getUserHome().getId());
		match.setUserHome(userHome);
		
		UserModel userAway = userService.findById(match.getUserAway().getId());
		match.setUserAway(userAway);
		
		// Goals
		match.getGoals().forEach(g -> g.setMatch(match));
		
		// Stats
		match.setStats(this.listStats(match));
		
		return matchRepository.save(match);
	} 
	
	public Page<Match> findByUserId(Long id, Pageable pageable) {
		Page<Match> matches = matchRepository.findByUserHomeIdOrUserAwayId(id, id, pageable);
		
		// Include total goals in matches
		this.totalGoals(matches.getContent());
		
		return matches;
	}
	
	public Page<RoundDTO> latestRounds(Long id, Pageable pageable) {
		return matchRepository.latestRounds(id, pageable);
	}
	
	/**
	 * Retorna a lista de estatisticas das partidas
	 * @param match
	 * @return
	 */
	private List<MatchStats> listStats(Match match) {
		List<MatchStats> listStats = new ArrayList<MatchStats>();
		MatchStats statsHome = new MatchStats();
		MatchStats statsAway = new MatchStats();
		
		statsHome.setMatch(match);
		statsAway.setMatch(match);
		statsHome.setUser(match.getUserHome());
		statsAway.setUser(match.getUserAway());
		
		int goalsHome = 0;
		int goalsAway = 0;
		for (MatchGoals g : match.getGoals()) {
			if (g.getUser().getId().equals(match.getUserHome().getId())) {
				goalsHome += g.getNumberGoals();
			} else {
				goalsAway += g.getNumberGoals();
			}
		}
		
		if (goalsHome > goalsAway) {
			statsHome.setMatchResult(1);
			statsAway.setMatchResult(0);
		} else if (goalsAway > goalsHome) {
			statsAway.setMatchResult(1);
			statsHome.setMatchResult(0);
		} else {
			statsHome.setMatchResult(2);
			statsAway.setMatchResult(2);
		}
		
		listStats.add(statsHome);
		listStats.add(statsAway);
		
		return listStats;
	}
	
	/**
	 * Inclui a contagem total de gols dentro de casa e fora de casa na partida
	 * @param matches
	 */
	private void totalGoals(List<Match> matches) {
		for (Match match : matches) {
			int goalsHome = 0;
			int goalsAway = 0;
			for (MatchGoals goal : match.getGoals()) {
				if (goal.getUser().getId().equals(match.getUserHome().getId())) {
					goalsHome += goal.getNumberGoals();
				} else {
					goalsAway += goal.getNumberGoals();
				}
			}
			match.setTotalGoalsHome(goalsHome);
			match.setTotalGoalsAway(goalsAway);
		}
	}
	
}
