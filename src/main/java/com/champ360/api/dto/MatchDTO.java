package com.champ360.api.dto;

import java.time.LocalDate;
import java.util.List;

public class MatchDTO {

	private Long id;
	private UserDTO userHome;
	private UserDTO userAway;
	private TeamDTO teamHome;
	private TeamDTO teamAway;
	private List<MatchGoalsDTO> goals;
	private List<MatchStatsDTO> stats;
	private Integer totalGoalsHome;
	private Integer totalGoalsAway;
	private LocalDate creationDate; 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserDTO getUserHome() {
		return userHome;
	}

	public void setUserHome(UserDTO userHome) {
		this.userHome = userHome;
	}

	public UserDTO getUserAway() {
		return userAway;
	}

	public void setUserAway(UserDTO userAway) {
		this.userAway = userAway;
	}

	public TeamDTO getTeamHome() {
		return teamHome;
	}

	public void setTeamHome(TeamDTO teamHome) {
		this.teamHome = teamHome;
	}

	public TeamDTO getTeamAway() {
		return teamAway;
	}

	public void setTeamAway(TeamDTO teamAway) {
		this.teamAway = teamAway;
	}

	public List<MatchGoalsDTO> getGoals() {
		return goals;
	}

	public void setGoals(List<MatchGoalsDTO> goals) {
		this.goals = goals;
	}
	
	public List<MatchStatsDTO> getStats() {
		return stats;
	}

	public void setStats(List<MatchStatsDTO> stats) {
		this.stats = stats;
	}

	public Integer getTotalGoalsHome() {
		return totalGoalsHome;
	}

	public void setTotalGoalsHome(Integer totalGoalsHome) {
		this.totalGoalsHome = totalGoalsHome;
	}

	public Integer getTotalGoalsAway() {
		return totalGoalsAway;
	}

	public void setTotalGoalsAway(Integer totalGoalsAway) {
		this.totalGoalsAway = totalGoalsAway;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}
	
}
