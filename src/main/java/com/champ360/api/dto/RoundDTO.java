package com.champ360.api.dto;

import java.math.BigDecimal;
import java.sql.Date;

public class RoundDTO {

	private Date date;
	private BigDecimal victories;
	private BigDecimal defeats;
	private BigDecimal draws;

	public RoundDTO(Date date, BigDecimal victories, BigDecimal defeats, BigDecimal draws) {
		this.date = date;
		this.victories = victories;
		this.defeats = defeats;
		this.draws = draws;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getVictories() {
		return victories;
	}

	public void setVictories(BigDecimal victories) {
		this.victories = victories;
	}

	public BigDecimal getDefeats() {
		return defeats;
	}

	public void setDefeats(BigDecimal defeats) {
		this.defeats = defeats;
	}
	
	public BigDecimal getDraws() {
		return draws;
	}

	public void setDraws(BigDecimal draws) {
		this.draws = draws;
	}

}
