package com.champ360.api.dto;

public class MatchGoalsDTO {

	private UserDTO user;
	private Integer numberGoals;

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public Integer getNumberGoals() {
		return numberGoals;
	}

	public void setNumberGoals(Integer numberGoals) {
		this.numberGoals = numberGoals;
	}

}
