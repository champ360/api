package com.champ360.api.dto;

public class TeamDTO {

	private String name;
	private String acronym;
	private OfficialLeagueDTO officialLeague;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public OfficialLeagueDTO getOfficialLeague() {
		return officialLeague;
	}

	public void setOfficialLeague(OfficialLeagueDTO officialLeague) {
		this.officialLeague = officialLeague;
	}
	
}
