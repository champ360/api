package com.champ360.api.dto;

public class MatchStatsDTO {

	private UserDTO user;
	private Integer matchResult;

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public Integer getMatchResult() {
		return matchResult;
	}

	public void setMatchResult(Integer matchResult) {
		this.matchResult = matchResult;
	}
	
}
