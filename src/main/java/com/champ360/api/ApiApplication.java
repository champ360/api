package com.champ360.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.champ360.api.config.property.Champ360ApiProperty;

@SpringBootApplication
@EnableConfigurationProperties(Champ360ApiProperty.class)
public class ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

}
