package com.champ360.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.champ360.api.dto.UserDTO;
import com.champ360.api.model.UserModel;
import com.champ360.api.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('read')")
	public List<UserDTO> list() {
		List<UserModel> users = userService.list();
		return users.stream()
			.map(user -> convertToDto(user))
			.collect(Collectors.toList());
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('read')")
	public ResponseEntity<UserDTO> findById(@PathVariable Long id) {
		UserModel userModel = userService.findById(id);
		UserDTO userDTO = convertToDto(userModel);
		return ResponseEntity.ok(userDTO);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public ResponseEntity<UserDTO> save(@Valid @RequestBody UserModel newUser) {
		UserModel userModel = userService.save(newUser);
		UserDTO userDTO = convertToDto(userModel);
		return ResponseEntity.status(HttpStatus.CREATED).body(userDTO);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public ResponseEntity<UserDTO> update(@PathVariable Long id, @Valid @RequestBody UserModel newUser) {
		UserModel userModel = userService.update(id, newUser);
		UserDTO userDTO = convertToDto(userModel);
		return ResponseEntity.ok(userDTO);
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public void delete(@PathVariable Long id) {
		userService.delete(id);
	}
	
	private UserDTO convertToDto(UserModel user) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(user, UserDTO.class);
	}
	
}