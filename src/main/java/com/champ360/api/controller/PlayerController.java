package com.champ360.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.champ360.api.dto.PlayerDTO;
import com.champ360.api.model.Player;
import com.champ360.api.service.PlayerService;

@RestController
@RequestMapping("/players")
public class PlayerController {

	@Autowired
	private PlayerService playerService;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('read')")
	public List<PlayerDTO> list() {
		List<Player> players = playerService.list();
		return players.stream()
			.map(player -> convertToDto(player))
			.collect(Collectors.toList());
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('read')")
	public ResponseEntity<PlayerDTO> findById(@PathVariable Long id) {
		Player player = playerService.findById(id);
		PlayerDTO playerDTO = convertToDto(player);
		return ResponseEntity.ok(playerDTO);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public ResponseEntity<PlayerDTO> save(@Valid @RequestBody Player newPlayer) {
		Player player = playerService.save(newPlayer);
		PlayerDTO playerDTO = convertToDto(player);
		return ResponseEntity.status(HttpStatus.CREATED).body(playerDTO);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public ResponseEntity<PlayerDTO> update(@PathVariable Long id, @Valid @RequestBody Player newPlayer) {
		Player player = playerService.update(id, newPlayer);
		PlayerDTO playerDTO = convertToDto(player);
		return ResponseEntity.ok(playerDTO);
	}
	
	private PlayerDTO convertToDto(Player player) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(player, PlayerDTO.class);
	}
	
}
