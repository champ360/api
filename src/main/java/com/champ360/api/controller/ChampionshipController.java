package com.champ360.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.champ360.api.dto.ChampionshipDTO;
import com.champ360.api.model.Championship;
import com.champ360.api.service.ChampionshipService;

@RestController
@RequestMapping("/championships")
public class ChampionshipController {

	@Autowired
	private ChampionshipService championshipService;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('read')")
	public List<ChampionshipDTO> list() {
		List<Championship> championships = championshipService.list();
		return championships.stream()
			.map(championship -> convertToDto(championship))
			.collect(Collectors.toList());
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('write')")
	public ResponseEntity<ChampionshipDTO> save(@Valid @RequestBody Championship newChampionship) {
		Championship championship = championshipService.save(newChampionship);
		ChampionshipDTO championshipDTO = convertToDto(championship);
		return ResponseEntity.status(HttpStatus.CREATED).body(championshipDTO);
	}
	
	private ChampionshipDTO convertToDto(Championship championship) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(championship, ChampionshipDTO.class);
	}
}
