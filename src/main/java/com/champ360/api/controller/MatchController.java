package com.champ360.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.champ360.api.dto.MatchDTO;
import com.champ360.api.dto.RoundDTO;
import com.champ360.api.model.Match;
import com.champ360.api.service.MatchService;

@RestController
@RequestMapping("/matches")
public class MatchController {

	@Autowired
	private MatchService matchService;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('read')")
	public List<MatchDTO> list() {
		List<Match> matches = matchService.list();
		return matches.stream()
			.map(match -> convertToDto(match))
			.collect(Collectors.toList());
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('write')")
	public ResponseEntity<MatchDTO> save(@Valid @RequestBody Match newMatch) {
		Match match = matchService.save(newMatch);
		MatchDTO matchDTO = convertToDto(match);
		return ResponseEntity.status(HttpStatus.CREATED).body(matchDTO);
	}
	
	@GetMapping("/find-by-user/{id}")
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('read')")
	public Page<MatchDTO> findByUserId(@PathVariable Long id, Pageable pageable) {
		Page<Match> list = matchService.findByUserId(id, pageable);
		return list.map(match -> convertToDto(match));
	}
	
	@GetMapping("/rounds/find-by-user/{id}")
	public Page<RoundDTO> findLatestRounds(@PathVariable Long id, Pageable pageable) {
		return matchService.latestRounds(id, pageable);
	}
	
	private MatchDTO convertToDto(Match match) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(match, MatchDTO.class);
	}
	
}
