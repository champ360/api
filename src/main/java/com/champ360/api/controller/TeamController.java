package com.champ360.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.champ360.api.dto.TeamDTO;
import com.champ360.api.model.Team;
import com.champ360.api.service.TeamService;

@RestController
@RequestMapping("/teams")
public class TeamController {

	@Autowired
	private TeamService teamService;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('read')")
	public List<TeamDTO> list() {
		List<Team> teams = teamService.list();
		return teams.stream()
			.map(team -> convertToDto(team))
			.collect(Collectors.toList());
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_USER') and #oauth2.hasScope('read')")
	public ResponseEntity<TeamDTO> findById(@PathVariable Long id) {
		Team team = teamService.findById(id);
		TeamDTO teamDTO = convertToDto(team);
		return ResponseEntity.ok(teamDTO);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public ResponseEntity<TeamDTO> save(@Valid @RequestBody Team newTeam) {
		Team team = teamService.save(newTeam);
		TeamDTO teamDTO = convertToDto(team);
		return ResponseEntity.status(HttpStatus.CREATED).body(teamDTO);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public ResponseEntity<TeamDTO> update(@PathVariable Long id, @Valid @RequestBody Team newTeam) {
		Team team = teamService.update(id, newTeam);
		TeamDTO teamDTO = convertToDto(team);
		return ResponseEntity.ok(teamDTO);
	}
	
	private TeamDTO convertToDto(Team team) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(team, TeamDTO.class);
	}
	
}
