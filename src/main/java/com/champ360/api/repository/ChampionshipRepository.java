package com.champ360.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.Championship;

public interface ChampionshipRepository extends JpaRepository<Championship, Long> {

}
