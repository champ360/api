package com.champ360.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.ChampionshipFormat;

public interface ChampionshipFormatRepository extends JpaRepository<ChampionshipFormat, Long> {

}
