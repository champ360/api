package com.champ360.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.ChampionshipVideogame;

public interface ChampionshipVideogameRepository extends JpaRepository<ChampionshipVideogame, Long> {

}
