package com.champ360.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {

	public Optional<UserModel> findByEmail(String email);
	
	public List<UserModel> findByRolesDescription(String permissaoDescricao);
	
}
