package com.champ360.api.repository.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.champ360.api.dto.RoundDTO;
import com.champ360.api.repository.custom.MatchRepositoryCustom;

public class MatchRepositoryImpl implements MatchRepositoryCustom {

	@PersistenceContext
	private EntityManager manager;

	@SuppressWarnings("unchecked")
	@Override
	public Page<RoundDTO> latestRounds(Long id, Pageable pageable) {
		String qlString = "SELECT m.creation_date,"
				+ " SUM(ms.match_result = 1),"
				+ " SUM(ms.match_result = 0),"
				+ " SUM(ms.match_result = 2)"
				+ " FROM match_stats ms INNER JOIN `match` m"
				+ " ON ms.id_match = m.id"
				+ " WHERE ms.id_user = :id"
				+ " GROUP BY m.creation_date";
		Query query = manager.createNativeQuery(qlString);
		query.setParameter("id", id);
		
		int totalRecords = query.getResultList().size(); 
		
		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		
		List<Object[]> list = query.getResultList();
		List<RoundDTO> listRounds = new ArrayList<RoundDTO>();
		for (Object[] obj: list) {
			RoundDTO round = new RoundDTO((Date) obj[0], (BigDecimal) obj[1], (BigDecimal) obj[2], (BigDecimal) obj[3]);
			listRounds.add(round);
		}
		
		return new PageImpl<>(listRounds, pageable, totalRecords);
	}
	
}
