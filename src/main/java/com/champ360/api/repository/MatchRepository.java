package com.champ360.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.Match;
import com.champ360.api.repository.custom.MatchRepositoryCustom;

public interface MatchRepository extends JpaRepository<Match, Long>, MatchRepositoryCustom {

	Page<Match> findByUserHomeIdOrUserAwayId(Long idUserHome, Long idUserAway, Pageable pageable);
	
	@EntityGraph(value = "match-load-championship", type = EntityGraphType.LOAD)
	List<Match> findAll();
	
}
