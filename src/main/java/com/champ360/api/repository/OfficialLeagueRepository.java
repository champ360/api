package com.champ360.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.OfficialLeague;

public interface OfficialLeagueRepository extends JpaRepository<OfficialLeague, Long> {

}
