package com.champ360.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.ChampionshipSystem;

public interface ChampionshipSystemRepository extends JpaRepository<ChampionshipSystem, Long> {

}
