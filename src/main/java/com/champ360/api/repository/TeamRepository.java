package com.champ360.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {

	
}
