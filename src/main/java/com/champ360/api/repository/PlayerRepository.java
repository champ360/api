package com.champ360.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.Player;

public interface PlayerRepository extends JpaRepository<Player, Long> {

}
