package com.champ360.api.repository.custom;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.champ360.api.dto.RoundDTO;

public interface MatchRepositoryCustom {
	
	Page<RoundDTO> latestRounds(Long id, Pageable pageable);
	
}
