package com.champ360.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.champ360.api.model.ChampionshipPlatform;

public interface ChampionshipPlatformRepository extends JpaRepository<ChampionshipPlatform, Long> {

}
